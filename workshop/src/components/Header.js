import React from "react";
import { NavLink } from 'react-router-dom'
export default function Header(props) {
  // const auth = localStorage.getItem('Id');
  // if (auth != '') {
  //   nav = 'user'
  // }else{
  //   nav = 'no'
  // }

  return (
    <div>
      <nav class="navbar navbar-expand-md navbar-light bg-dark">
        <NavLink to="/" className="navbar-brand" style={{ color: '#F1948A'}}> BLACKPINK</NavLink>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar7"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="navbar-collapse collapse justify-content-stretch"
          id="navbar7"
        >
          <ul class="navbar-nav ml-auto">

          <li class="nav-item">
             <NavLink to="#"onClick={() =>  localStorage.removeItem('Id')} className="nav-link"style={{ color: '#F1948A'}}>LOG OUT</NavLink>
            </li>
          <li class="nav-item">

              <NavLink to="/profile" className="nav-link" activeStyle={{color: 'black',backgroundColor:'#F1948A'}} style={{ color: '#F1948A'}}>PROFILE</NavLink>
            </li>
            <li class="nav-item">
              <NavLink to="/login" className="nav-link" activeStyle={{color: 'black',backgroundColor:'#F1948A'}} style={{ color: '#F1948A'}}>LOG IN</NavLink>
            </li>
            <li class="nav-item">
              <NavLink to="/register" className="nav-item nav-link" activeStyle={{color: 'black',backgroundColor:'#F1948A'}} style={{ color: '#F1948A'}}>REGRIST</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
