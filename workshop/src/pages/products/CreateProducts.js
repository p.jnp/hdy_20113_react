import React, { useState } from "react";
import Back from "../../components/Back";
import {createProduct} from '../../api/Api'
import ProductForm from '../../components/products/ProductForm'
export default function CreateProducts(props) {
    // var id = localStorage.getItem('Id');
    // const [user_id, setID] = useState('')
    // const [title, setTitle] = useState('')
    // const [detail, setDetail] = useState('')
    // const [stock, setStock] = useState(0)
    // const [price, setPrice] = useState(0)
    // const create = async (e) => {
    //     e.preventDefault();
    //     let product ={
    //         user_id: id,
    //         title: title,
    //         detail: detail,
    //         stock: stock,
    //         price: price
    //     }
    const create = async (product) => {
        let result = await createProduct(product)
            props.history.push('/home')
      }

  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "20px" }}>
        CREATE BLACKPINK PRODUCTS
      </h1>
      <Back url="/home" history={props.history} />
     <ProductForm create={create} />
    </div>
  );
}
