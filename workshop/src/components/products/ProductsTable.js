import React, { useState } from "react";
import { Link } from "react-router-dom";

export default function ProductsTable(props) {
  var id = localStorage.getItem("Id");

  const tableList = () => {

    if (props.product.data !== undefined) {
      var data = [];
      for (let i = 0; i < props.product.data.length; i++) {
        let item = props.product.data[i];
        if (props.check === "All") {
          data.push(
            <tr>
              <th scope="row">{i + 1} </th>

              {/* <td>{item.user_id}</td> */}
              <td>{item.title}</td>
              <td>{item.detail}</td>
              <td>{item.stock}</td>
              <td>{item.price}</td>
            </tr>
          );
        } else if (item.user_id === id) {
          
          data.push(
            <tr>
              <th scope="row">{i + 1}</th>
              {/* <td>{item._id}</td> */}
              {/* <td>{item.user_id}</td> */}
              <td>{item.title}</td>
              <td>{item.detail}</td>
              <td>{item.stock}</td>
              <td>{item.price}</td>
              <td>
                <Link to={`/editproduct/${item._id}`}>
                  <span
                    style={{
                      color: "#F1948A",
                      backgroundColor: "black",
                      padding: "8px",
                      marginRight: "2px",
                    }}
                  >
                    Edit
                  </span>
                </Link>
                
                <Link>
                  <span
                    onClick={() => props.delete(item._id)}
                    style={{
                      color: "black",
                      backgroundColor: "#F1948A",
                      padding: "8px",
                    }}
                  >
                    Delete
                  </span>
                </Link>
              </td>
            </tr>
          );
        }
      }
      return data;
    }
  };
  return (
    <div>
      {props.check === "All" ? (
        <table
          className="table"
          style={{
            marginTop: "20px",
          }}
        >
          <thead className="thead-dark">
            <tr>
              <th scope="col" style={{ color: "#F1948A" }}>
                No
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                title
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                detail
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                stock
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                price
              </th>
            </tr>
          </thead>
          <tbody>{tableList()}</tbody>
        </table>
      ) : (
        <table
          className="table"
          style={{
            marginTop: "20px",
          }}
        >
          <thead className="thead-dark">
            <tr>
              <th scope="col" style={{ color: "#F1948A" }}>
                No
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                title
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                detail
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                stock
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                price
              </th>
              <th scope="col" style={{ color: "#F1948A" }}>
                Actions
              </th>
            </tr>
          </thead>
          <tbody>{tableList()}</tbody>
        </table>
      )}
    </div>
  );
}
