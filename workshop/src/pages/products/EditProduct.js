import React, { useEffect, useState } from "react";
import Back from '../../components/Back'
import ProductForm from '../../components/products/ProductForm'
import { getIDProducts,getAllProducts,editProduct } from '../../api/Api'


export default function EditProduct(props) {
  
  const [product, setProduct] = useState();

  useEffect(() => {
    const fetchApi = async () => {
      let result = await getAllProducts(props.match.params.id);
     
      // console.log('id'+props.match.params.id)
      let data = result.data.filter((item) => {
        console.log(item)
       return item._id === props.match.params.id;

      });
      setProduct(data[0])
      // console.log('data[]'+data[0])
    };
    fetchApi();
  }, []);

  
  const edit = async (product) => {
    let edit = await editProduct(props.match.params.id, product)
    console.log(edit.status)
    if (edit.status === "success") {
      props.history.push('/home')
    } else{
        alert(edit.message)

    }
  }
  

  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "20px" }}>
       EDIT BLACKPINK PRODUCTS
 
      </h1>
      <Back url="/home" history={props.history} />
   
      {product !== undefined && 
      <ProductForm check="Edit" product={product} edit={edit} />
      }
    </div>
  );
}
