import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Profile from "../../pages/users/Profile";
import ProductsTable from "../../components/products/ProductsTable";
import { getAllProducts,deleteProduct} from "../../api/Api";

export default function Home() {
  var id = localStorage.getItem("Id");
  const [product, setProduct] = useState({});
  useEffect(() => {
    fetchProduct();
  }, []);
  const fetchProduct = async () => {
    let result = await getAllProducts();
    setProduct(result);
  };
  const removeProduct  = async (id) => {
    let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
    let result = await deleteProduct(id)
    if (result.status === "success") {
    fetchProduct ()
    }
    }
    }
  return (
    <div>
      {/* {id} */}
      <Link
        to={`/product`}
        className="float-right btn-bp"
        style={{ marginTop: "10px" }}
      >
        <span>Creat Product</span>
      </Link>
      <h3 style={{ textAlign: "center", marginTop: "20px", marginBottom:"20px" }}>
        Welcome To My BLINK WORLD
      </h3>

      <nav>
        <div
          className="nav nav-tabs justify-content-center"
          id="nav-tab"
          role="tablist"
        >
          <a
            className="nav-item nav-link active"
            id="nav-home-tab"
            data-toggle="tab"
            href="#nav-all"
            role="tab"
            aria-controls="nav-home"
            aria-selected="true"
          >
            ALL BLINK PRODUCT
          </a>
          <a
            className="nav-item nav-link"
            id="nav-profile-tab"
            data-toggle="tab"
            href="#nav-id"
            role="tab"
            aria-controls="nav-profile"
            aria-selected="false"
          >
            BLINK PRODUCT
          </a>
        </div>
      </nav>
      <div className="tab-content" id="nav-tabContent">
        <div
          className="tab-pane fade show active"
          id="nav-all"
          role="tabpanel"
          aria-labelledby="nav-all-tab"
        >
          <ProductsTable check="All" product={product} />
        </div>
        <div
          className="tab-pane fade"
          id="nav-id"
          role="tabpanel"
          aria-labelledby="nav-id-tab"
        >

          <ProductsTable product={product} delete={removeProduct}/>
        </div>
      </div>
    </div>
  );
}
