import React, { useState, useEffect } from "react";
import Back from "../../components/Back";
import { getUserById, editUser } from "../../api/Api";
import ProfileForm from "../../components/users/ProfileForm";
export default function EditProfile(props) {
  const [user, setUser] = useState();

  useEffect(() => {
    const fetchApi = async () => {
      let result = await getUserById(props.match.params.id);
      setUser(result.data);
    };
    fetchApi();
  }, []);

  const edit = async (user) => {
    let edit = await editUser(props.match.params.id, user);
    if (edit.status === "success") {
      props.history.push("/profile");
    }
    alert("แก้ไขข้อมูลสำเร็จ");
  };

  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "20px" }}>
        EDIT BLINK PROFILE
      </h1>
      <Back url="/profile" history={props.history} />
      {user !== undefined && 
      <ProfileForm check="Edit" user={user} edit={edit} />
      }
    </div>
  );
}
