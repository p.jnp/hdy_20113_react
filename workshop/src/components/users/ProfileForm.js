import React, { useState, useEffect } from "react";

export default function ProfileForm(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [salary, setSalary] = useState(0);

  const save = async (e) => {
    e.preventDefault();
    let user = {
      username: username,
      password: password,
      name: name,
      age: age,
      salary: salary
    };
    props.save(user);
  };

  const edit = async (e) => {
    e.preventDefault();
    let user = {
        name: name,
        age: age,
        salary: salary,
    };
    props.edit(user);
  };

  useEffect(() => {
    if (props.check === "Edit") {
        setUsername(props.user.username);
        setPassword(props.user.password);
      setName(props.user.name);
      setAge(props.user.age);
      setSalary(props.user.salary);
    }
  }, []);

  return (
    <div>
      <form onSubmit={props.check === "Edit" ? edit : save} style={{  marginTop: "20px" }}>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Username:</label>
            <input
              type="text"
              class="form-control"
              id="username"
              placeholder="กรุณาป้อน username ของคุณ"
              onChange={(e) => setUsername(e.target.value)}
              value={username}
              disabled
            />
          </div>
          <div class="form-group col-md-6">
            <label>Password:</label>
            <input
              type="password"
              class="form-control"
              id="password"
              placeholder="กรุณาป้อน password ของคุณ"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              disabled
            />
          </div>
        </div>
        <div class="form-group">
          <label>Name:</label>
          <input
            type="text"
            class="form-control"
            id="name"
            placeholder="กรุณาป้อน name ของคุณ"
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Age:</label>
            <input
              type="number"
              class="form-control"
              id="age"
              placeholder="กรุณาป้อน age ของคุณ"
              onChange={(e) => setAge(e.target.value)}
              value={age}
            />
          </div>
          <div class="form-group col-md-6">
            <label>Salary:</label>
            <input
              type="number"
              class="form-control"
              id="salary"
              placeholder="กรุณาป้อน salary ของคุณ"
              onChange={(e) => setSalary(e.target.value)}
              value={salary}
            />
          </div>
        </div>

        <button type="submit" class=" btn-bp float-right">
          save
        </button>
      </form>
    </div>
  );
}
