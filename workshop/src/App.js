import React from "react";
import "./App.css";
import { Route, Switch, Redirect } from "react-router-dom";
import PrivateRoute from "./helper/PrivateRoute";
import PageNotFound from "./pages/PageNotFound";
import Header from "./components/Header";
import Login from "./pages/Login";
import Register from "./pages/Register";
import HeaderUsers from "./components/HeaderUsers";
import Home from "./pages/users/Home";
import Profile from "./pages/users/Profile";
import EditProfile from "./pages/users/EditProfile";
import CreateProducts from "./pages/products/CreateProducts";
import EditProduct from './pages/products/EditProduct'
var routes = {
  pagenotfound: '/*',
  login: '/login',
  register: '/register',
  home: '/home',
  profile: '/profile',
  editprofile: '/editprofile/:id',
  product: '/product',
  editproduct:'/editproduct/:id'
};
function App(props) {
  return (
    <div>
      {/* {(auth!= '' ? <HeaderUsers /> : <Header />)} */}
      <Header />
      <div className="container">
        <Switch>
          <Redirect exact from="/" to={routes.home}></Redirect>
          <Route exact path={routes.login} component={Login}></Route>
          <Route exact path={routes.register} component={Register}></Route>

          <PrivateRoute
            exact
            path={routes.home}
            component={Home}
          ></PrivateRoute>

          <PrivateRoute
            exact
            path={routes.profile}
            component={Profile}
          ></PrivateRoute>

          <PrivateRoute
            exact
            path={routes.editprofile}
            component={EditProfile}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.product}
            component={CreateProducts}
          ></PrivateRoute>
          <PrivateRoute
            exact
            path={routes.editproduct}
            component={EditProduct}
          ></PrivateRoute>

          <Route
            exact
            path={routes.pagenotfound}
            component={PageNotFound}
          ></Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
