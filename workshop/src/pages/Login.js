import React, { useState } from "react";
import { loginTest } from "./../api/Api";
import { setTokenLogin } from "./../Token/AuthToken"
export default function Login(props) {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const login = async (e) => {
      e.preventDefault();
      let user = {
        username: username,
        password: password
      };

  

    let result = await loginTest(user);
    // props.history.push('/home')
    if (result.status === 'success') {
      localStorage.setItem('Id', result.data._id);
      localStorage.setItem('Status', result.data.status);
      // setTokenLogin(result.data._id);
      props.history.push("/home");
      console.log(result.data._id);
    } else {
      alert("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง");
      console.log(result.status);
    }
  };
  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "180px" }}>LOG IN</h1>
      <div className="login">
        {/* <LoginForm login={login} /> */}
        <form onSubmit={ login }>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Username:</label>
            <input
              type="text"
              class="form-control"
              name="username"
              id="username"
              placeholder="กรุณาป้อน username ของคุณ"
              onChange={(e) => setUserName(e.target.value)}
            />
          </div>
          <div class="form-group col-md-6">
            <label>Password:</label>
            <input
              type="password"
              class="form-control"
              name="password"
              id="password"
              placeholder="กรุณาป้อน password ของคุณ"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
        </div>

        <button type="submit" class="btn-bp float-right">
          LOG IN
        </button>
      </form>
      </div>
    </div>
  );
}
