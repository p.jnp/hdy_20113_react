import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Back from '../../components/Back'
import { getUserById } from "../../api/Api";
export default function Profile(props) {
  const [userProfile, setUserProfile] = useState([]);
  var id = localStorage.getItem('Id');
  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    await getUserById(id).then((res) => {
      if (res.status === "success") {
        setUserProfile(res.data);
      }
    });
  };
  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "20px" }}>BLINK PROFILE </h1>
      
      <Back url="/home" history={props.history} />
      <Link to={`/editprofile/${userProfile._id}`} className="btn-bp float-right" style={{position:'relative',top:'-30px'}}>
        <span
        >
          Edit Profile
        </span>
      </Link>
      <div style={{ margin: "40px 0 0 100px" }}>
        <div className="row">
          <div className="col-md-6">
            <label
              style={{
                color: "#F1948A",
                fontSize: "20px",
                fontWeight: "bold",
              }}
            >
              Username:
            </label>
            <p
              style={{
                paddingLeft: "15px",
              }}
            >
              {userProfile.username}
            </p>
          </div>
          <div className="col-md-6">
            <label
              style={{
                color: "#F1948A",
                fontSize: "20px",
                fontWeight: "bold",
              }}
            >
              Password:
            </label>
            <p
              style={{
                paddingLeft: "15px",
              }}
            >
              {userProfile.password}
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <label
              style={{
                color: "#F1948A",
                fontSize: "20px",
                fontWeight: "bold",
              }}
            >
              Name:
            </label>
            <p
              style={{
                paddingLeft: "15px",
              }}
            >
              {userProfile.name}
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <label
              style={{
                color: "#F1948A",
                fontSize: "20px",
                fontWeight: "bold",
              }}
            >
              Age:
            </label>
            <p
              style={{
                paddingLeft: "15px",
              }}
            >
              {userProfile.age}
            </p>
          </div>
          <div className="col-md-6">
            <label
              style={{
                color: "#F1948A",
                fontSize: "20px",
                fontWeight: "bold",
              }}
            >
              Salary:
            </label>
            <p
              style={{
                paddingLeft: "15px",
              }}
            >
              {userProfile.salary}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
