import React, { useState} from "react";
import { register } from './../api/Api'
import ProfileForm from '../components/users/ProfileForm'
export default function Register(props) {
    const save = async (user) => {
        let result = await register(user)
        props.history.push('/login')
        
      }
  return (
    <div>
      <h1 style={{ textAlign: "center", marginTop: "100px" }}>REGRISTER</h1>
      <div className="login">
      <ProfileForm save={save} />
      </div>
    </div>
  );
}
