import React, { useState, useEffect } from "react";

export default function ProductForm(props) {

    var id = localStorage.getItem('Id');
    const [user_id, setID] = useState('')
    const [title, setTitle] = useState('')
    const [detail, setDetail] = useState('')
    const [stock, setStock] = useState(0)
    const [price, setPrice] = useState(0)
    const create = async (e) => {
        e.preventDefault();
        let product ={
            user_id: id,
            title: title,
            detail: detail,
            stock: stock,
            price: price
        }
        props.create(product);
    }

    useEffect(() => {
      if (props.check === "Edit") {
        setID(props.product.u_id);
        setTitle(props.product.title);
        setDetail(props.product.detail);
        setStock(props.product.stock);
        setPrice(props.product.price);
      }
    }, []);

    const edit = async (e) => {
        e.preventDefault();
        let product = {
            user_id: id,
            title: title,
            detail: detail,
            stock: stock,
            price: price
        };
        props.edit(product);
      };
    

    return (
        <div>
             <form onSubmit={props.check === "Edit" ? edit : create} style={{ marginTop: "20px" }}>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label>User_ID:</label>
            <input
              type="text"
              class="form-control"
              id="user_id"
              value={id}
            //   onChange={(e) => setUsername(e.target.value)}
              disabled
            />
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label>Title:</label>
            <input
              type="text"
              class="form-control"
              id="title"
              placeholder="กรุณาป้อน title ของคุณ"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              
            />
          </div>
        </div>
        <div class="form-group">
          <label>Detail:</label>
          <textarea
            class="form-control"
            id="detail"
            rows="3"
            placeholder="กรุณาป้อน detail ของคุณ"
            
            value={detail}
            onChange={(e) => setDetail(e.target.value)}
          ></textarea>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Stock:</label>
            <input
              type="number"
              class="form-control"
              id="stock"
              placeholder="กรุณาป้อน stock ของคุณ"
              value={stock}
              onChange={(e) => setStock(e.target.value)}
            />
          </div>
          <div class="form-group col-md-6">
            <label>Price:</label>
            <input
              type="number"
              class="form-control"
              id="price"
              placeholder="กรุณาป้อน price ของคุณ"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
        </div>

        <button type="submit" class=" btn-bp float-right">
          Save
        </button>
      </form>
        </div>
    )
}
